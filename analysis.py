# Load libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

csv_file = './apple_stock_file.csv'
cols = ['Day', 'Day Name', 'Month', 'Year', 'Open', 'High', 'Low', 'Close/Last', 'Volume']
dataset = pd.read_csv(csv_file)
dfObj = pd.DataFrame(dataset, columns=['Day', 'Day Name', 'Month', 'Year', 'Open', 'High', 'Low', 'Close/Last', 'Volume'])
subsetDataFrame2018 = dfObj[dfObj['Year'] == 2018]
subsetDataFrame2018Months = subsetDataFrame2018['Month'].values.tolist()
subsetDataFrame2018Data = subsetDataFrame2018['Close/Last'].values.tolist()
# list2018 = list(subsetDataFrame2018)
# list2018data = list(subsetDataFrame2018Data)
# print(subsetDataFrame2018)
print(subsetDataFrame2018Data)
# years = dataset['Year'].tolist()
# open_data = dataset.loc['Open']
# high_data = dataset.loc['High']
# low_data = dataset.loc['Low']
# close_data = dataset['Close/Last'].tolist()
#
plt.plot(subsetDataFrame2018Months[::-1], subsetDataFrame2018Data[::-1], 'b-', label='Close/Last Data')
plt.rcParams["figure.figsize"] = (100,50)
plt.legend(loc='upper left')
plt.xlabel('Month')
plt.ylabel('Close/Last Data')
plt.show()