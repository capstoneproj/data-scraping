from bs4 import BeautifulSoup  # Library function for BeautifulSoup
from calendar import weekday, day_name  # Library functions to get weekday and day name corresponding to the date
import csv  # Library to implement csv functions
from requests import get  # Library function to get data from url (website)

# Library function to print any exception that caused during getting the data from url
from requests.exceptions import RequestException
from contextlib import closing  # Library function to close the connection to url once data is retrieved

data = []  # an empty temporary list where we will store the data we retrieve
raw_html = ''  # variable to get raw format of html data scrapped
stock_symbol = ''
# lists corresponding to respective columns to include in csv file
day, date, dayName, open_data, high_data, low_data, close_last, volume, year, month = [], [], [], [], [], [], [], [], [], []

print("Select any one of the stocks to scrap the data: ")
print("1. Apple     - AAPL")
print("2. Microsoft - MSFT")
print("3. Amazon    - AMZN")
print("4. IBM       - IBM")
print("5. Facebook  - FB")
selection = input("Enter your choice: ")
if selection == '1':
    stock_symbol = "AAPL"
elif selection == '2':
    stock_symbol = "MSFT"
elif selection == '3':
    stock_symbol = "AMZN"
elif selection == '4':
    stock_symbol = "IBM"
elif selection == '5':
    stock_symbol = "FB"

# the url from where we are scraping the data
url = 'http://charting.nasdaq.com/ext/charts.dll?2-1-14-0-0-5120-03NA000000' + stock_symbol + '-&SF:4|5-WD=539-HT=395--XTBL-'

# try except block to get the content only if the status code is 200 i.e. OK and there is at least one HTML tag
try:
    with closing(get(url, stream=True)) as resp:
        content_type = resp.headers['Content-Type'].lower()
        if resp.status_code == 200 and content_type is not None and content_type.find('html') > -1:
            raw_html = resp.content
        else:
            raw_html = None
except RequestException as e:  # catching the error and printing them
    print('Error during requests to {0} : {1}'.format(url, str(e)))

# using BeautifulSoup constructor to initialize the object with raw_html as one parameter i.e. raw data and other is the
# parser called html.parser
html = BeautifulSoup(raw_html, 'html.parser')

# finding the table from html (we parsed above) which has class named "DrillDown"
table = html.find('table', {"class": "DrillDown"})

# finding every tr tag in table
for tr in table.select('tr'):

    # finding every td tag in tr
    for td in range(len(tr.select('td'))):
        # appending the data from td to temporary list data
        data.append(tr.select('td')[td].text)
    # if there is no data, we will ignore the loop
    if not data:
        pass
    # else appending the data we received in respective lists
    else:
        # spliting the date accroding to dd,mm,yyy format to get the weekday name using weekday function
        dayNumber = weekday(int(data[0].split("/")[2]), int(data[0].split("/")[0]), int(data[0].split("/")[1]))
        dayName.append(day_name[dayNumber])
        month.append(data[0].split("/")[0])
        day.append(int(data[0].split("/")[1]))
        year.append(int(data[0].split("/")[2]))
        date.append(data[0])
        open_data.append(float(data[1]))
        high_data.append(float(data[2]))
        low_data.append(float(data[3]))
        close_last.append(float(data[4]))
        volume.append(data[5])
        data = []

# opening the apple_stock_file.csv file in writing mode to write the data we scrapped
with open(stock_symbol.lower() + '_stock_file.csv', mode='w', newline='') as stock_file:
    # assigning the csv writer to write the data only which contains the quotechar and delimeter
    stock_file_writer = csv.writer(stock_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # writing the columns name in the csv file
    stock_file_writer.writerow(
        ['Date', 'Day', 'Day Name', 'Month', 'Year', 'Open', 'High', 'Low', 'Close/Last', 'Volume'])

    # writing all the data from list in the csv file corresponding to the columns name
    for i in range(len(day)):
        stock_file_writer.writerow(
            [date[i], day[i], dayName[i], month[i], year[i], open_data[i], high_data[i], low_data[i], close_last[i], volume[i]])